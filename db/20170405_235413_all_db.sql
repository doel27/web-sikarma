#
# TABLE STRUCTURE FOR: online_users
#

DROP TABLE IF EXISTS `online_users`;

CREATE TABLE `online_users` (
  `session` varchar(100) NOT NULL,
  `time` int(11) DEFAULT NULL,
  PRIMARY KEY (`session`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `online_users` (`session`, `time`) VALUES ('542d6aed5d98e2092a081612a32d81f1bb21b2ff', '1491411227');
INSERT INTO `online_users` (`session`, `time`) VALUES ('83ec7b7ad9cccfaec690ecadf20f720cd76701dc', '1491411253');
INSERT INTO `online_users` (`session`, `time`) VALUES ('9769941653e520d1e006b325595ecda4ddce059f', '1491411201');


#
# TABLE STRUCTURE FOR: wd_data_relation
#

DROP TABLE IF EXISTS `wd_data_relation`;

CREATE TABLE `wd_data_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `on_delete` enum('restrict','cascade') NOT NULL DEFAULT 'restrict',
  `primary_table` varchar(100) NOT NULL,
  `primary_id` varchar(100) NOT NULL,
  `relation_table` varchar(100) NOT NULL,
  `relation_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

INSERT INTO `wd_data_relation` (`id`, `on_delete`, `primary_table`, `primary_id`, `relation_table`, `relation_id`) VALUES ('19', 'cascade', 'wd_modules', 'id', 'wd_group_privileges', 'modules_id');
INSERT INTO `wd_data_relation` (`id`, `on_delete`, `primary_table`, `primary_id`, `relation_table`, `relation_id`) VALUES ('5', 'restrict', 'wd_groups', 'id', 'wd_users_groups', 'group_id');
INSERT INTO `wd_data_relation` (`id`, `on_delete`, `primary_table`, `primary_id`, `relation_table`, `relation_id`) VALUES ('6', 'restrict', 'wd_users', 'id', 'wd_users_groups', 'user_id');
INSERT INTO `wd_data_relation` (`id`, `on_delete`, `primary_table`, `primary_id`, `relation_table`, `relation_id`) VALUES ('7', 'restrict', 'wd_groups', 'id', 'wd_group_privileges', 'groups_id');


#
# TABLE STRUCTURE FOR: wd_group_privileges
#

DROP TABLE IF EXISTS `wd_group_privileges`;

CREATE TABLE `wd_group_privileges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groups_id` mediumint(8) unsigned NOT NULL,
  `modules_id` int(11) NOT NULL,
  `privilege` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_wb_group_privilege_wb_groups1_idx` (`groups_id`),
  KEY `fk_wb_group_privilege_wb_modules1_idx` (`modules_id`),
  CONSTRAINT `fk_wb_group_privilege_wb_groups1` FOREIGN KEY (`groups_id`) REFERENCES `wd_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_wb_group_privilege_wb_modules1` FOREIGN KEY (`modules_id`) REFERENCES `wd_modules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('1', '1', '1', '11000');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('2', '1', '2', '11000');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('3', '1', '3', '11111');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('4', '1', '4', '11111');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('5', '1', '5', '11111');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('6', '1', '6', '11111');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('7', '1', '7', '11000');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('8', '1', '8', '11000');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('9', '1', '9', '11111');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('10', '1', '10', '11111');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('11', '2', '1', '11111');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('12', '2', '2', '11111');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('13', '2', '3', '11111');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('14', '2', '4', '11111');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('15', '2', '5', '11111');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('16', '2', '6', '11111');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('17', '2', '9', '11111');
INSERT INTO `wd_group_privileges` (`id`, `groups_id`, `modules_id`, `privilege`) VALUES ('18', '2', '10', '11111');


#
# TABLE STRUCTURE FOR: wd_groups
#

DROP TABLE IF EXISTS `wd_groups`;

CREATE TABLE `wd_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `no_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `wd_groups` (`id`, `name`, `description`, `no_delete`) VALUES ('1', 'super_admin', 'For Developer Only', '1');
INSERT INTO `wd_groups` (`id`, `name`, `description`, `no_delete`) VALUES ('2', 'admin', 'Administrator', '1');
INSERT INTO `wd_groups` (`id`, `name`, `description`, `no_delete`) VALUES ('3', 'member', 'Member', '1');
INSERT INTO `wd_groups` (`id`, `name`, `description`, `no_delete`) VALUES ('4', 'operator', 'Operator', '1');


#
# TABLE STRUCTURE FOR: wd_login_attempts
#

DROP TABLE IF EXISTS `wd_login_attempts`;

CREATE TABLE `wd_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: wd_migration
#

DROP TABLE IF EXISTS `wd_migration`;

CREATE TABLE `wd_migration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) DEFAULT NULL,
  `date` datetime NOT NULL,
  `mode` varchar(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: wd_modules
#

DROP TABLE IF EXISTS `wd_modules`;

CREATE TABLE `wd_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `icon` varchar(50) NOT NULL DEFAULT 'fa fa-circle-o',
  `url` varchar(45) DEFAULT '#',
  `parent` varchar(45) DEFAULT NULL,
  `support` varchar(5) DEFAULT '00000' COMMENT 'xxxxx\n\n1 = Admin\n1 = Read\n1 = Create\n1 = Update\n1 = Delete',
  `sort_order` int(11) DEFAULT NULL,
  `only_super` int(11) DEFAULT NULL,
  `no_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO `wd_modules` (`id`, `title`, `icon`, `url`, `parent`, `support`, `sort_order`, `only_super`, `no_delete`) VALUES ('1', 'Dahsboard', 'fa fa-dashboard', 'dashboard', '0', '10000', '1', '1', '1');
INSERT INTO `wd_modules` (`id`, `title`, `icon`, `url`, `parent`, `support`, `sort_order`, `only_super`, `no_delete`) VALUES ('2', 'System', 'fa fa-gear', '#', '0', '10000', '2', '0', '1');
INSERT INTO `wd_modules` (`id`, `title`, `icon`, `url`, `parent`, `support`, `sort_order`, `only_super`, `no_delete`) VALUES ('3', 'Users', 'fa fa-user-plus', '#', '2', '11111', '3', '0', '1');
INSERT INTO `wd_modules` (`id`, `title`, `icon`, `url`, `parent`, `support`, `sort_order`, `only_super`, `no_delete`) VALUES ('4', 'User', 'fa fa-user', 'users', '3', '11111', '4', '0', '1');
INSERT INTO `wd_modules` (`id`, `title`, `icon`, `url`, `parent`, `support`, `sort_order`, `only_super`, `no_delete`) VALUES ('5', 'Groups', 'fa fa-users', 'groups', '3', '11111', '5', '0', '1');
INSERT INTO `wd_modules` (`id`, `title`, `icon`, `url`, `parent`, `support`, `sort_order`, `only_super`, `no_delete`) VALUES ('6', 'Module', 'fa fa-terminal', 'module', '2', '11111', '6', '0', '1');
INSERT INTO `wd_modules` (`id`, `title`, `icon`, `url`, `parent`, `support`, `sort_order`, `only_super`, `no_delete`) VALUES ('7', 'Data Relation', 'fa fa-archive', 'relation', '2', '10000', '7', '1', '1');
INSERT INTO `wd_modules` (`id`, `title`, `icon`, `url`, `parent`, `support`, `sort_order`, `only_super`, `no_delete`) VALUES ('8', 'Migration', 'fa fa-hourglass-1', 'migration', '2', '10000', '8', '1', '1');
INSERT INTO `wd_modules` (`id`, `title`, `icon`, `url`, `parent`, `support`, `sort_order`, `only_super`, `no_delete`) VALUES ('9', 'Backup DB', 'fa fa-database', 'backup', '2', '11111', '9', '0', '1');
INSERT INTO `wd_modules` (`id`, `title`, `icon`, `url`, `parent`, `support`, `sort_order`, `only_super`, `no_delete`) VALUES ('10', 'Options', 'fa fa-bars', 'options', '2', '11111', '10', '0', '1');


#
# TABLE STRUCTURE FOR: wd_options
#

DROP TABLE IF EXISTS `wd_options`;

CREATE TABLE `wd_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `value` text,
  `only_super` int(11) NOT NULL DEFAULT '0',
  `no_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

INSERT INTO `wd_options` (`id`, `name`, `value`, `only_super`, `no_delete`) VALUES ('1', 'admin_email', 'admin@admin.com', '1', '1');
INSERT INTO `wd_options` (`id`, `name`, `value`, `only_super`, `no_delete`) VALUES ('2', 'super_admin_group', 'super_admin', '1', '1');
INSERT INTO `wd_options` (`id`, `name`, `value`, `only_super`, `no_delete`) VALUES ('3', 'admin_group', 'admin', '1', '1');
INSERT INTO `wd_options` (`id`, `name`, `value`, `only_super`, `no_delete`) VALUES ('4', 'default_group', 'member', '1', '1');
INSERT INTO `wd_options` (`id`, `name`, `value`, `only_super`, `no_delete`) VALUES ('5', 'identity', 'email', '1', '1');
INSERT INTO `wd_options` (`id`, `name`, `value`, `only_super`, `no_delete`) VALUES ('6', 'password_email', '123456', '1', '1');
INSERT INTO `wd_options` (`id`, `name`, `value`, `only_super`, `no_delete`) VALUES ('7', 'warranty app name', 'nama aplikasi', '0', '1');
INSERT INTO `wd_options` (`id`, `name`, `value`, `only_super`, `no_delete`) VALUES ('8', 'warranty Licence', 'lisensi', '1', '1');
INSERT INTO `wd_options` (`id`, `name`, `value`, `only_super`, `no_delete`) VALUES ('9', 'warranty guarantee', 'garansi', '1', '1');
INSERT INTO `wd_options` (`id`, `name`, `value`, `only_super`, `no_delete`) VALUES ('10', 'warranty release', 'release date', '1', '1');


#
# TABLE STRUCTURE FOR: wd_users
#

DROP TABLE IF EXISTS `wd_users`;

CREATE TABLE `wd_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `no_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `wd_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `no_delete`) VALUES ('1', '127.0.0.1', 'super', '$2y$08$s6Ctz9s9wBfAJM1Mf/hZAelLiVOa4MtZUG.4EzD.dl2WJFOaRvaIG', '', 'support@indonesiait.com', '', NULL, NULL, 'Vqh1rHuSW4qI0snfe1E4.u', '1268889823', '1488425598', '1', 'Super', 'Admin', 'ADMIN', '', '1');
INSERT INTO `wd_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `no_delete`) VALUES ('2', '127.0.0.1', 'admin', '$2y$08$AIiuasVsa4kyBAOhuh7HXOkBeN4TThXF3TjnlrIfxEZyw8bFnk99G', NULL, 'admin@admin.com', NULL, NULL, NULL, 'AWO6WnUKaIx4vFHcPG/JZu', '1465717020', '1466497168', '1', 'Administrator', '', '', '', '1');


#
# TABLE STRUCTURE FOR: wd_users_groups
#

DROP TABLE IF EXISTS `wd_users_groups`;

CREATE TABLE `wd_users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `wd_groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `wd_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `wd_users_groups` (`id`, `user_id`, `group_id`) VALUES ('1', '1', '1');
INSERT INTO `wd_users_groups` (`id`, `user_id`, `group_id`) VALUES ('2', '2', '2');
INSERT INTO `wd_users_groups` (`id`, `user_id`, `group_id`) VALUES ('3', '2', '3');


